import os
import json
import re
import datetime
import flask
from flask_restful import Resource, Api, abort

app = flask.Flask(__name__)
api = Api(app)

class Welcome(Resource):
  def get(self):
    return(ok(None, "Welcome to Kristians remote control app"))

class Opentab(Resource):
  def get(self):
    authenticate()
    link=getlink()
    openlink(link)
    return(ok("OK. Tried to launch link '%s' in new tab" % link))

class Image(Resource):
  def get(self):
    authenticate()
    wnr=getwnr()
    showImage(wnr)
    return(ok("OK. I will try to show you the image of w%s" % wnr))

api.add_resource(Welcome, '/')
api.add_resource(Opentab, '/opentab')
api.add_resource(Image, '/image')

def authenticate():
  auth = flask.request.headers.get('AUTH-TOKEN')
  if auth is None:
    auth = flask.request.args.get("auth")
  if auth is None:
    error(403, "I need some credentials before I can let you in...")
  if (auth!="testsecret"):
    error(403, "That does not grant you access to here...")
  return(True)

def getlink():  
  link = flask.request.values.get('link')
  if link is None:
    error(400, "I need something to work with here! Give me a link")
  link = fixlink(link)
  if not validate_link(link):
    error(400, "That is one god damn ugly link. I don't trust that...: %s" % link)
  return(link)

def fixlink(link):
  link=link.strip('\"').strip("'")
  if link[0:4]=="http": return(link)
  return("https://"+link)

def validate_link(link):
  regex=r"^http[s]?://([-\w]*\.)+[a-z]{2,}(/[-\w]+)*/?(\?([-\w\[\]]+=[-\w]*)?(&[-\w\[\]]+=[-\w]*)*)?$"
  m=re.match(regex, link)
  return(re.match(regex, link))

def openlink(link):
  os.system("cmd.exe /c 'c:\kristian\scripts\\newtab.bat' \"%s\"" % link)


def getwnr():
  wnr = flask.request.values.get("wnr")
  if not wnr: error(400, "You need to specify wnr")
  if wnr[0:1]=="w" or wnr[0:1]=="W":
    wnr=wnr[1:]
  regex=r"^[0-9]+$"
  if not re.match(regex, wnr): error(400, "Wnr must be numeric (It is OK to start with w or W)")
  wnr=wnr.zfill(5)
  return(wnr)

def showImage(wnr):
  print("Showing image of %s" % wnr)
  os.system("cmd.exe /c 'c:\kristian\scripts\showimage.bat' %s" % wnr)

def error(code=500, message="Something went wrong :-("):
  t=timestamp()
  abort(code, message=message, status="error", code=code, time=t["time"], timestr=t["timestr"])

def ok(result=None, message="Request recieved"):
  obj = {"status":"ok", "code": 200, "message": message}
  obj.update(timestamp())
  if result is not None:
    obj["result"]=result
  return(obj)

def timestamp(outformat="human"):
  if outformat=="human": outformat="%Y-%m-%d %H:%M:%S"
  if outformat=="unix":  outformat="%s"
  timeobj=datetime.datetime.now()
  timestamp=timeobj.strftime("%s")
  timestring=timeobj.strftime("%Y-%m-%d %H:%M:%S")
  return {"time": timestamp, "timestr": timestring}

if __name__ == '__main__':
  app.run(debug=True)

